package digregorio.dario.de.restclient.proxy;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import digregorio.dario.de.restclient.administration.ContractAdministration;
import digregorio.dario.de.restclient.model.Contract;

/**
 * Proxy Klasse die Aufrufe Zum REST Server machen
 * Methoden und JavaDoc aus ContractAdministration
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public class ContractProxy implements ContractAdministration {
    private final String url = "http://192.168.178.20:8080/";

    @Override
    public List<Contract> getContracts() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ResponseEntity<Contract[]> responseEntity = restTemplate.getForEntity(url + "contracts", Contract[].class);
        return Arrays.asList(responseEntity.getBody());
    }

    @Override
    public Contract getContractById(String contractId) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        return restTemplate.getForObject(url + "contracts/" + contractId, Contract.class);
    }

    @Override
    public void addContract(String type, Long contribution, String customerId) {
        Contract body = new Contract(type, contribution);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Contract> entity = new HttpEntity<>(body, header);
        // send request and parse result
        restTemplate.exchange(url + "customers/" + customerId + "/contracts", HttpMethod.POST, entity, String.class);
    }

    @Override
    public void updateContract(String type, Long contribution, String contractId, String customerId) {
        Contract body = new Contract(type, contribution);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Contract> entity = new HttpEntity<>(body, header);
        // send request and parse result
        restTemplate.exchange(url + "customers/" + customerId + "/contracts/" + contractId, HttpMethod.PUT, entity, String.class);
    }

    @Override
    public void deleteContract(String customerId, String contractId) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Contract> entity = new HttpEntity<>(header);
        // send request and parse result
        restTemplate.exchange(url + "customers/" + customerId + "/contracts/" + contractId, HttpMethod.DELETE, entity, String.class);
    }
}
