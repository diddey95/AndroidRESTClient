package digregorio.dario.de.restclient.proxy;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import digregorio.dario.de.restclient.administration.CustomerAdministration;
import digregorio.dario.de.restclient.model.Address;
import digregorio.dario.de.restclient.model.Customer;

/**
 * Proxy Klasse die Aufrufe Zum REST Server machen
 * Methoden und JavaDoc aus CustomerAdministration
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public class CustomerProxy implements CustomerAdministration {
    private final String url = "http://192.168.178.20:8080/";

    @Override
    public List<Customer> getCustomers() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ResponseEntity<Customer[]> responseEntity = restTemplate.getForEntity(url + "customers", Customer[].class);
        return Arrays.asList(responseEntity.getBody());
    }

    @Override
    public Customer getCustomerById(String customerId) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        return restTemplate.getForObject(url + "customers/" + customerId, Customer.class);
    }

    @Override
    public void addCustomer(String name, String surname, String birthday, Address address) {
        Customer body = new Customer(name, surname, birthday);
        body.setAddress(address);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Customer> entity = new HttpEntity<>(body, header);
        // send request and parse result
        restTemplate.exchange(url + "customers", HttpMethod.POST, entity, String.class);
    }

    @Override
    public void updateCustomer(String name, String surname, String birthday, String customerId) {
        Customer body = new Customer(name, surname, birthday);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Customer> entity = new HttpEntity<>(body, header);
        // send request and parse result
        restTemplate.exchange(url + "customers/" + customerId, HttpMethod.PUT, entity, String.class);
    }

    @Override
    public void deleteCustomer(String customerId) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Customer> entity = new HttpEntity<>(header);
        // send request and parse result
        restTemplate.exchange(url + "customers/" + customerId, HttpMethod.DELETE, entity, String.class);
    }

    @Override
    public void addAddress(String street, Integer postcode, String town, String customerId) {
        Address body = new Address(street, postcode, town);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Address> entity = new HttpEntity<>(body, header);
        // send request and parse result
        restTemplate.exchange(url + "customers/" + customerId + "/addresses", HttpMethod.POST, entity, String.class);
    }

    @Override
    public void updateAddress(String street, Integer postcode, String town, String customerId) {
        Address body = new Address(street, postcode, town);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Address> entity = new HttpEntity<>(body, header);
        // send request and parse result
        restTemplate.exchange(url + "customers/" + customerId + "/addresses", HttpMethod.PUT, entity, String.class);
    }

    @Override
    public void deleteAddress(String customerId) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Address> entity = new HttpEntity<>(header);
        // send request and parse result
        restTemplate.exchange(url + "customers/" + customerId + "/addresses", HttpMethod.DELETE, entity, String.class);
    }
}
