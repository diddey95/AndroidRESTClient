package digregorio.dario.de.restclient;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import digregorio.dario.de.restclient.proxy.ContractProxy;
import digregorio.dario.de.restclient.proxy.CustomerProxy;

/**
 * Verteilte Verarbeitung SS 2017
 * Praktikum Übung 3
 * RESTful WebService Client Android
 * <p>
 * Folgende Funktionen hat der Client:
 * - Spring Boot Android Client
 * - Spring Templates um HTTP Aufrufe zu machen
 * - Kommuniziert mit Server
 * - Schnittstelle um passende GUI zu kreieren
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {

    private final String LOG_TAG = MainActivity.class.getSimpleName();

    //Erstellt Layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    //Wird beim Start ausgeführt
    @Override
    protected void onStart() {
        super.onStart();
        new HttpRequestTask().execute();
    }

    //Erstellt Menü zum Refreshen
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    //Wenn Refreshed wird, wird Http Request Task ausgeführt
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            new HttpRequestTask().execute();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Enthält View
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.activity_main, container, false);
        }
    }

    /**
     * Da Androids UI in der Main Thread ausgeführt wird und ständig aktualisiert wird.
     * Müssen andere Methoden in einem (Hintergrund) Thread ausgeführt werden damit UI nicht
     * unterbrochen wird.
     */
    private class HttpRequestTask extends AsyncTask<Void, Void, Void> {
        //Methoden um HTTP aufrufe zu testen
        @Override
        protected Void doInBackground(Void... params) {
            try {
                CustomerProxy customerProxy = new CustomerProxy();
                ContractProxy contractProxy = new ContractProxy();

                //Liest Customer 1 aus Repository
                Log.v(LOG_TAG, "successfully read customer by id:\n " + customerProxy.getCustomerById("1").toString());

                //Liest alle Customers
                Log.v(LOG_TAG, "successfully read customers:\n " + customerProxy.getCustomers().toString());

                //Fügt Customer hinzu
                customerProxy.addCustomer("Dario", "Didgeridu", "08.07.1995", null);
                Log.v(LOG_TAG, "successfully add customer:\n " + customerProxy.getCustomerById("4"));

                //Aktualisiert Customer
                customerProxy.updateCustomer("Dario", "Digregorio", "08.07.1995", "4");
                Log.v(LOG_TAG, "successfully update customer:\n " + customerProxy.getCustomerById("4"));

                //Löscht Customer
                customerProxy.deleteCustomer("2");
                Log.v(LOG_TAG, "successfully delete customer 2:\n " + customerProxy.getCustomers());

                //Fügt Address hinzu
                customerProxy.addAddress("Heinz-Otto-Str.21", 83022, "Rosenheim", "4");
                Log.v(LOG_TAG, "successfully add address:\n " + customerProxy.getCustomerById("4"));

                //Aktualisiert Address
                customerProxy.updateAddress("Heinz-Otto-Str.21", 83022, "München", "4");
                Log.v(LOG_TAG, "successfully add address:\n " + customerProxy.getCustomerById("4"));

                //Löscht Address
                customerProxy.deleteAddress("1");
                Log.v(LOG_TAG, "successfully delete address from customer 1:\n " + customerProxy.getCustomerById("1"));

                //Fügt 4 Contracts hinzu
                contractProxy.addContract("Haftpflicht", Long.valueOf("30000"), "4");
                contractProxy.addContract("Lebensversicherung", Long.valueOf("22222"), "3");
                contractProxy.addContract("Handyversicherung", Long.valueOf("30000"), "4");
                contractProxy.addContract("Unfallversicherung", Long.valueOf("1500"), "1");
                Log.v(LOG_TAG, "successfully add and read 4 contracts:\n " + contractProxy.getContracts());

                //Liest Contract aus Customer 4
                Log.v(LOG_TAG, "successfully read contract:\n " + contractProxy.getContractById("4"));

                //Aktualisiert Contract
                contractProxy.updateContract("Haftpflicht", Long.valueOf("40000"), "1", "4");
                Log.v(LOG_TAG, "successfully update contract:\n " + contractProxy.getContractById("1"));

                //Löscht Contract
                contractProxy.deleteContract("4", "1");
                Log.v(LOG_TAG, "successfully delete contract from customer 4:\n " + customerProxy.getCustomerById("4"));

                return null;
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage(), e);
            }
            return null;
        }
    }

}