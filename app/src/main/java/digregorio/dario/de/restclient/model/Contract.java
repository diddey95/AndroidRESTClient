package digregorio.dario.de.restclient.model;


import java.util.Date;

/**
 * Contract Klasse um entsprechende Objekte zu parsen
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public class Contract {

    private Long contractId;
    private String type;
    private Long contribution;
    private Date version;

    public Contract(String type, Long contribution) {
        this.type = type;
        this.contribution = contribution;
    }

    public Contract() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getContribution() {
        return contribution;
    }

    public void setContribution(Long contribution) {
        this.contribution = contribution;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Date getVersion() {
        return version;
    }

    public void setVersion(Date version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contract contract = (Contract) o;

        return contractId != null ? contractId.equals(contract.contractId) : contract.contractId == null
                && (type != null ? type.equals(contract.type) : contract.type == null
                && (contribution != null ? contribution.equals(contract.contribution) : contract.contribution == null
                && (version != null ? version.equals(contract.version) : contract.version == null)));

    }

    @Override
    public int hashCode() {
        int result = contractId != null ? contractId.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (contribution != null ? contribution.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "contractId=" + contractId +
                ", type='" + type + '\'' +
                ", contribution=" + contribution +
                ", version=" + version +
                '}';
    }
}
