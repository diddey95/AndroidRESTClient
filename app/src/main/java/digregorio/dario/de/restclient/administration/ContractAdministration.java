package digregorio.dario.de.restclient.administration;


import java.util.List;

import digregorio.dario.de.restclient.model.Contract;

/**
 * Interface mit Methoden für die Proxy Klasse Contract
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public interface ContractAdministration {
    /**
     * Bekommt alle Contracts in einer List
     * @return List<Cotnracts>
     */
    List<Contract> getContracts();

    /**
     * Bekommt Contract mit ID
     * @param contractId Contract ID
     * @return Contract
     */
    Contract getContractById(String contractId);

    /**
     * Fügt Contract hinzu
     * @param type Contract Typ
     * @param contribution Contract Jahresbetrag
     * @param customerId Customer ID
     */
    void addContract (String type, Long contribution, String customerId);

    /**
     * Aktualisiert Cotnract
     * @param type Contract Typ
     * @param contribution Contract Jahresbeitrag
     * @param contractId Contract ID
     * @param customerId Customer ID
     */
    void updateContract (String type, Long contribution, String contractId, String customerId);

    /**
     * Löscht Contract
     * @param customerId Customer ID
     * @param contractId Contract ID
     */
    void deleteContract (String customerId, String contractId);
}
