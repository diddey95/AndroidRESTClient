package digregorio.dario.de.restclient.administration;


import java.util.List;

import digregorio.dario.de.restclient.model.Address;
import digregorio.dario.de.restclient.model.Customer;

/**
 * Interface mit Methoden für die Proxy Klasse Customer
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public interface CustomerAdministration {
    /**
     * Bekommt alle Customers in einer List
     *
     * @return List<Customer>
     */
    List<Customer> getCustomers();

    /**
     * Bekommt Customer mit ID
     *
     * @param customerId Customer ID
     * @return Customer
     */
    Customer getCustomerById(String customerId);

    /**
     * Fügt Customer hinzu
     *
     * @param name     Customer name
     * @param surname  Customer nachname
     * @param birthday Customer Geburtstag
     * @param address  Customer Adresse
     */
    void addCustomer(String name, String surname, String birthday, Address address);

    /**
     * Aktualisiert Customer
     *
     * @param name       Customer Name
     * @param surname    Customer Nachname
     * @param birthday   Custoemr Geburtstag
     * @param customerId Customer ID
     */
    void updateCustomer(String name, String surname, String birthday, String customerId);

    /**
     * Löscht Customer
     *
     * @param customerId Customer ID
     */
    void deleteCustomer(String customerId);

    /**
     * Fügt Address zu Customer
     *
     * @param street     Address Straße
     * @param postcode   Address PLZ
     * @param town       Address Stadt
     * @param customerId Customer ID
     */
    void addAddress(String street, Integer postcode, String town, String customerId);

    /**
     * Aktualisiert Address
     *
     * @param street     Address Straße
     * @param postcode   Address PLZ
     * @param town       Address Stadt
     * @param customerId Cusotmer ID
     */
    void updateAddress(String street, Integer postcode, String town, String customerId);

    /**
     * Löscht Address
     *
     * @param customerId Customer ID
     */
    void deleteAddress(String customerId);
}
