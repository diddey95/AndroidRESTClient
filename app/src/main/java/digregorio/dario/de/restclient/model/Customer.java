package digregorio.dario.de.restclient.model;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Customer Klasse um entsprechende Objekte zu parsen
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public class Customer {
    private Long customerId;

    private String name;
    private String surname;
    private String birthday;
    private Address address;
    private List<Contract> contracts = new LinkedList<>();
    private Date version;

    public Customer() {

    }

    public Customer(String name, String surname, String birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Date getVersion() {
        return version;
    }

    public void setVersion(Date version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return customerId != null ? customerId.equals(customer.customerId) : customer.customerId == null
                && (name != null ? name.equals(customer.name) : customer.name == null
                && (surname != null ? surname.equals(customer.surname) : customer.surname == null
                && (birthday != null ? birthday.equals(customer.birthday) : customer.birthday == null
                && (address != null ? address.equals(customer.address) : customer.address == null
                && (contracts != null ? contracts.equals(customer.contracts) : customer.contracts == null
                && (version != null ? version.equals(customer.version) : customer.version == null))))));

    }

    @Override
    public int hashCode() {
        int result = customerId != null ? customerId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (contracts != null ? contracts.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthday='" + birthday + '\'' +
                ", address=" + address +
                ", contracts=" + contracts +
                ", version=" + version +
                '}';
    }
}
