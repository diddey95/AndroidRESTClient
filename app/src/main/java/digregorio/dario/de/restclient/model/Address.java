package digregorio.dario.de.restclient.model;


/**
 * Address Klasse um entsprechende Objekte zu parsen
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public class Address {
    private String street;
    private Integer postcode;
    private String town;

    public Address(String street, Integer postcode, String town) {
        this.street = street;
        this.postcode = postcode;
        this.town = town;
    }

    public Address() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getPostcode() {
        return postcode;
    }

    public void setPostcode(Integer postcode) {
        this.postcode = postcode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }
}
